#
# Be sure to run `pod lib lint XMDevelopmentUtil.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'XMDevelopmentUtil'
  s.version          = '0.0.2'
  s.summary          = 'xmitech development util.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'xmitech software development kit'

  s.homepage         = 'https://gitlab.com/TixXie/XMDevelopmentUtil'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'Apache License 2.0', :file => 'LICENSE' }
  s.author           = { 'TixXie' => 'x.maple@hotmail.com' }
  s.source           = { :git => 'https://gitlab.com/TixXie/XMDevelopmentUtil.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform         = :ios, "9.0"
  # s.ios.deployment_target = '11.0'

  # s.source_files = 'XMDevelopmentUtil/Classes/**/*'
  
  # s.resource_bundles = {
  #   'XMDevelopmentUtil' => ['XMDevelopmentUtil/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
  s.subspec 'XMMQTTClient' do |m|
    m.vendored_frameworks = "XMDevelopmentUtil/XMMQTTClient/*"
  end
  
  s.subspec 'XMBLESession' do |b|
    b.vendored_frameworks = "XMDevelopmentUtil/XMBLESession/*"
  end
  
  s.subspec 'XMMultiP2PLink' do |p|
    p.vendored_frameworks = "XMDevelopmentUtil/XMMultiP2PLink/*"
    p.dependency 'TIoTLinkKit_IJKPlayer'
  end
  
end

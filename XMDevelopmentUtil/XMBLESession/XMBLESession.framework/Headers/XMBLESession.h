//
//  XMBLESession.h
//  XMBLESession
//
//  Created by Tix Xie on 2022/1/4.
//

#import <Foundation/Foundation.h>
#import <XMBLESession/XMBLEManager.h>
#import <XMBLESession/XMBLESessionConstants.h>

NS_ASSUME_NONNULL_BEGIN

/// 版本号
FOUNDATION_EXPORT const unsigned char XMBLESessionVersionString[];

@interface XMBLESession : NSObject
/// 蓝牙状态
/// ⚠️注意：状态必须是XMBLESessionStatePoweredOn才能开始扫描
@property (nonatomic, readonly, assign) XMBLESessionState state;
/// 蓝牙连接状态
/// ⚠️注意：状态必须是XMBLESessionConnectStateReady才能开始向设备写入数据
@property (nonatomic, readonly, assign) XMBLESessionConnectState connectState;
/// 蓝牙授权状态
@property (nonatomic, readonly, assign) XMBLESessionAuthorization authorization API_AVAILABLE(ios(13.0));
/// 扫描中
@property (nonatomic, readonly, assign, getter=isScanning) BOOL scanning;
/// 断开自动重连，默认为NO， 调用disconnect后置为NO，所以下次需要重新设置
@property (nonatomic, assign, getter=isAutoReconnect) BOOL autoReconnect;
/// 当前连接的蓝牙名称
@property (nonatomic, readonly, copy) NSString *currentName;

/// 预初始化。预初始化是为了让蓝牙状态尽快进入XMBLESessionStatePoweredOn状态
+ (void)preInit;

/// 设置输出日志等级，默认是XMBLELogLevelWarn
/// @param level 等级
+ (void)setLogLevel:(XMBLELogLevel)level;

/// 单例
+ (instancetype)shared;

/// 扫描附近蓝牙设备
/// 扫描结果在XMBLESessionDidDiscoverPeripheralNotification通知中回调
- (void)startScan;

/// 同上，但指定扫描某些Service UUID的蓝牙
- (void)startScanWithServices:(NSArray <NSString *> *)services;

/// 停止扫描
- (void)stopScan;

/// 连接蓝牙设备
/// @param identifier 需要连接的蓝牙设备UUID，从XMBLESessionDidDiscoverPeripheralNotification回调通知中获得
/// @param timeout 超时时间，单位秒
- (void)connectIdentifier:(NSString *)identifier timeout:(NSTimeInterval)timeout;

/// 用于断开连接之后重连到上一个连接
- (BOOL)reconnectToLast;

/// 获取当前已连接设备的单次最大写入长度，单位：Byte
- (NSUInteger)maximumWriteValueLength;

/// 向已连接的蓝牙设备写入数据（设备发送给App的数据在XMBLESessionDidUpdateCustomizeValueNotification通知中获得）
/// @param value 需要写入的数据
/// @param timeout 超时时间，单位秒
/// @param callback 写入结果回调，error有值代表写入失败
- (void)writeCustomizeValue:(NSData *)value timeout:(NSTimeInterval)timeout callback:(void(^)(NSError *error))callback;

/// 断开连接
- (void)disconnect;

#pragma mark - 废弃方法
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END

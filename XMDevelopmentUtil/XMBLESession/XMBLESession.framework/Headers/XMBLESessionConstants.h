//
//  XMBLESessionConstants.h
//  XMBLESession
//
//  Created by Tix Xie on 2022/1/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - XMBLESession回调通知
/**
 * 蓝牙状态更新通知
 * ⚠️注意：状态必须是XMBLESessionStatePoweredOn才能开始扫描
 * state 蓝牙状态
 * Notification中的userInfo结构如下：
@code
{
    "state" = 0;  // state为NSNumber对象，对应XMBLESessionState枚举
}
@endCode
 */
FOUNDATION_EXPORT NSString * const XMBLESessionDidUpdateStateNotification;

/**
 * 蓝牙连接状态更新通知
 * ⚠️注意：状态必须是XMBLESessionConnectStateReady才能开始向设备写入数据
 * identifier 蓝牙设备UUID
 * connectState 连接状态
 * Notification中的userInfo结构如下：
@code
{
    "identifier" = "";      // identifier为NSString对象
    "connectState" = 0;     // connectState为NSNumber对象，对应XMBLESessionConnectState枚举
}
@endCode
 */
FOUNDATION_EXPORT NSString * const XMBLESessionDidUpdateConnectStateNotification;

/**
 * 扫描附近蓝牙设备回调通知
 * name 蓝牙设备名称
 * identifier 蓝牙设备的id
 * RSSI 蓝牙设备信号
 * advertisementData 蓝牙广播数据
 * Notification中的userInfo结构如下：
@code
{
    "identifier" = "";          // identifier为NSString对象
    "name" = "";                // name为NSString对象，可能为空
    "RSSI" = 0;                 // RSSI为NSNumber对象，可能为空
    "advertisementData" = {};   // advertisementData为NSDictionary对象，可能为空
}
@endCode
 */
FOUNDATION_EXPORT NSString * const XMBLESessionDidDiscoverPeripheralNotification;

/**
 * 收到设备数据回调通知
 * value 收到的数据
 * error  有值代表收到错误数据
 * Notification中的userInfo结构如下：
@code
{
    "value" = NSData;   // value为NSData对象，可能为空
    "error" = NSError;  // error为NSError对象，可能为空
}
@endCode
 */
FOUNDATION_EXPORT NSString * const XMBLESessionDidUpdateCustomizeValueNotification;

/**
 * 连接断开通知
 * identifier 蓝牙设备UUID
 * error 有值代表非主动断开
 * Notification中的userInfo结构如下：
@code
{
    "identifier" = "";  // identifier为NSString对象
    "error" = NSError;  // error为NSError对象，可能为空
}
@endCode
 */
FOUNDATION_EXPORT NSString * const XMBLESessionDidDisconnectIdentifierNotification;


#pragma mark - XMBLESession回调通知
/// XMBLEManager连接已经准备完成的通知
FOUNDATION_EXPORT NSString * const XMBLEManagerDidConnectReadyNotification;

/// 设备发送LOG的通知，userInfo中是设备传输过来的数据
FOUNDATION_EXPORT NSString * const XMBLEManagerDidReceivedLogNotification;


#pragma mark - 枚举
typedef NS_ENUM(NSInteger, XMBLESessionState) {
    /// 蓝牙状态未知
    XMBLESessionStateUnknown = 0,
    /// 蓝牙与系统服务的连接暂时丢失并重置中
    XMBLESessionStateResetting,
    /// 当前iPhone/iPad不支持蓝牙
    XMBLESessionStateUnsupported,
    /// 没有蓝牙权限
    XMBLESessionStateUnauthorized,
    /// 蓝牙已关闭
    XMBLESessionStatePoweredOff,
    /// 蓝牙已开启并可使用
    XMBLESessionStatePoweredOn,
};

typedef NS_ENUM(NSInteger, XMBLESessionAuthorization) {
    /// 用户尚未对此应用程序做出选择
    XMBLESessionAuthorizationNotDetermined = 0,
    /// 此应用程序无权使用蓝牙。 用户无法更改此应用程序的状态，可能是由于实施了家长控制等主动限制。
    XMBLESessionAuthorizationRestricted,
    /// 用户已明确拒绝此应用程序使用蓝牙。
    XMBLESessionAuthorizationDenied,
    /// 用户已授权此应用程序始终使用蓝牙。
    XMBLESessionAuthorizationAllowedAlways
} NS_ENUM_AVAILABLE(10_15, 13_0);

typedef NS_ENUM(NSInteger, XMBLESessionConnectState) {
    /// 初始状态
    XMBLESessionConnectStateNone = 0,
    /// 连接中
    XMBLESessionConnectStateConnecting,
    /// 连接完成
    XMBLESessionConnectStateConnected,
    /// 已经准备好可以向设备发数据
    XMBLESessionConnectStateReady,
    /// 连接超时
    XMBLESessionConnectStateTimeout,
    /// 连接失败
    XMBLESessionConnectStateConnectFailed,
    /// 断开连接
    XMBLESessionConnectStateDisconnected,
};

typedef NS_ENUM(NSInteger, XMBLESessionErrorCode) {
    /// 表示成功
    XMBLESessionErrorCodeSuccess             = 0,
    /// 超时
    XMBLESessionErrorCodeTimeout             = -1001,
    /// 断开连接
    XMBLESessionErrorCodeDisconnect          = -1002,
    /// 参数为空
    XMBLESessionErrorCodeParameterEmpty      = -1003,
    /// 连接没有准备好
    XMBLESessionErrorCodeNotReady            = -1004,
    /// 数据长度超出限制
    XMBLESessionErrorCodeLengthExceedsLimit  = -1005,
    /// JSON序列化失败
    XMBLESessionErrorCodeSerializationError  = -1006,
    /// 写入失败
    XMBLESessionErrorCodeWriteValueError     = -1007,
};

/// 日志输出等级
typedef NS_ENUM(NSInteger, XMBLELogLevel) {
    /// 调试日志
    XMBLELogLevelDebug  = 0,
    /// 一般日志
    XMBLELogLevelInfo   = 1,
    /// 警告日志
    XMBLELogLevelWarn   = 2,
    /// 错误日志
    XMBLELogLevelError  = 3,
};

/// 绑定错误码
typedef NS_ENUM(NSInteger, XMBLEBindErrorCode) {
    /// 绑定成功
    XMBLEBindErrorCodeSuccess       = 0,
    /// SPI异常 or 只获取到一个SSID
    XMBLEBindErrorCodeSPI           = 100001,
    /// DSP（比如T40）异常
    XMBLEBindErrorCodeDSP           = 100002,
    /// WiFi密码错误
    XMBLEBindErrorCodePassword      = 100003,
    /// WiFi连接超时
    XMBLEBindErrorCodeTimeout       = 100004,
    /// 参数错误
    XMBLEBindErrorCodeParameter     = 100005,
    /// HTTP通讯失败
    XMBLEBindErrorCodeHTTP          = 100006,
    /// 服务检查失败
    XMBLEBindErrorCodeCheck         = 100007,
    /// MQTT连接超时
    XMBLEBindErrorCodeMqttTimeout   = 100008,
    /// 获取P2P信息超时
    XMBLEBindErrorCodeP2PTimeout    = 100009,
    /// P2P信息错误
    XMBLEBindErrorCodeP2PInfo       = 1000010,
    /// 保存参数超时
    XMBLEBindErrorCodeSaveTimeout   = 1000011,
    /// WiFi参数校验失败
    XMBLEBindErrorCodeWiFiParameter = 1000012,
    /// 配置同步失败
    XMBLEBindErrorCodeSync          = 1000013,
    /// 已经被绑定
    XMBLEBindErrorCodeAlreadyBound  = 1000014,
};

#pragma mark - 回调对象
/// 结果回调对象
@interface XMResponse : NSObject

/// 结果回调block
typedef void (^XMResultResponse)(XMResponse *res);
/// 结果信息
@property (nonatomic, copy) NSString *msg;
/// 错误码，0是成功，其他失败
@property (nonatomic, assign) XMBLESessionErrorCode code;
/// 收到的负载数据
@property (nonatomic, copy) NSDictionary *body;
/// 是否成功
@property (nonatomic, assign, getter=isSuccess) BOOL success;

- (instancetype)initWithCode:(XMBLESessionErrorCode)code msg:(NSString *)msg success:(BOOL)success;

@end

NS_ASSUME_NONNULL_END

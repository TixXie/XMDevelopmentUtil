//
//  XMBLEManager.h
//  XMBLESession
//
//  Created by Tix Xie on 2022/1/12.
//

#import <Foundation/Foundation.h>
#import <XMBLESession/XMBLESessionConstants.h>

typedef NS_ENUM(NSInteger, XMBLEBindProgressStep) {
    /// 未知信息
    XMBLEBindProgressStepUnknown        = -1,
    /// 开始连接WiFi
    XMBLEBindProgressStepConnectWiFi    = 0,
    /// 主控读取设备信息(SN等)
    XMBLEBindProgressStepReadInfo       = 1,
    /// 获取MQTT服务器
    XMBLEBindProgressStepGetMQTT        = 2,
    /// 连接MQTT服务器
    XMBLEBindProgressStepConnectMQTT    = 3,
    /// 获取绑定信息
    XMBLEBindProgressStepGetBindInfo    = 4,
    /// 保存绑定信息
    XMBLEBindProgressStepSaveBindInfo   = 5,
    /// 上报绑定结果
    XMBLEBindProgressStepReportResult   = 6
};

NS_ASSUME_NONNULL_BEGIN

/// 蓝牙默认服务UUID
FOUNDATION_EXPORT NSString * const XMBLEManagerDefaultServiceUUID;

@protocol XMBLEManagerDelegate <NSObject>
@optional
/// 收到wifi信息回调
/// @param payload wifi信息
- (void)didReceiveWiFiInfo:(NSDictionary *)payload;

/// 更新步骤信息
/// @param step 当前进行的步骤
/// @param payload 全部数据
- (void)updateProgressStep:(XMBLEBindProgressStep)step payload:(NSDictionary *)payload;

/// 绑定结果回调
/// @param success 绑定成功或者绑定失败
/// @param code 0 是成功，其他失败
/// @param sn 设备sn，绑定成功时有值
/// @param msg 错误信息，可能为空
- (void)didReceiveBindResult:(BOOL)success code:(XMBLEBindErrorCode)code sn:(NSString *)sn msg:(NSString *)msg;
@end

/// 对XMBLESession的封装，回调通知(比如蓝牙状态等)请监听XMBLESessionConstants.h中的Notification
@interface XMBLEManager : NSObject

/// 代理回调
@property (nonatomic, weak) id <XMBLEManagerDelegate> delegate;

/// 写入数据的超时时间。默认是10秒
@property (nonatomic, assign) NSUInteger writeTimeout;

/// 设置参数回调的超时时间，就是下面有XMResultResponse回调的方法。默认是10秒
/// ⚠️注意：最大超时时间应该是writeTimeout + callbackTimeout，因为每个设置都需要写入数据，所以需要加上writeTimeout
@property (nonatomic, assign) NSUInteger callbackTimeout;

/// 断开自动重连，默认为NO， 调用disconnect后置为NO，所以下次连接需要重新设置
@property (nonatomic, assign, getter=isAutoReconnect) BOOL autoReconnect;

/// 预初始化。预初始化是为了让蓝牙状态尽快进入XMBLESessionStatePoweredOn状态
+ (void)preInit;

/// 设置输出日志等级，默认是XMBLELogLevelWarn
/// @param level 等级
+ (void)setLogLevel:(XMBLELogLevel)level;

/// 单例
+ (instancetype)manager;

#pragma mark - 蓝牙相关
/// 扫描附近蓝牙设备
/// 扫描结果在XMBLESessionDidDiscoverPeripheralNotification通知中获取
- (void)startScan;
/// 同上，但指定扫描某些Service UUID的蓝牙
- (void)startScanWithServices:(NSArray <NSString *> *)services;

/// 停止扫描
- (void)stopScan;

/// 连接蓝牙设备
/// @param identifier 需要连接的蓝牙设备UUID，从XMBLESessionDidDiscoverPeripheralNotification回调中获得
/// @param timeout 超时时间，单位秒
- (void)connectIdentifier:(NSString *)identifier timeout:(NSTimeInterval)timeout;

/// 用于断开连接之后重连到上一个连接
- (BOOL)reconnectToLast;

/// 断开蓝牙连接
- (void)disconnect;

#pragma mark - 设置相关 ⚠️注意：callback在回调之前会一直强引用，所以在callback里面建议使用weakSelf。除非调用disconnect会强制释放
/// 获取版本
/// @param callback 结果回调
- (void)getVersionWithCallback:(XMResultResponse)callback;

/// 扫描WiFi列表
/// @param timeout 超时时间，秒。建议30秒
/// @param callback 结果回调。如果成功，wifi信息在代理didReceivedWiFiInfo中回调
- (void)scanWifiListWithTimeout:(NSUInteger)timeout callback:(XMResultResponse)callback;

/// 停止扫描WiFi列表
/// 这里实际上不是让设备停止扫描，而是不回调。因为设备在扫描完成之前无法停止
- (void)stopScanWiFi;

/// 获取WiFi状态
/// @param callback 结果回调
- (void)getWiFiStatusWithCallback:(XMResultResponse)callback;

/// 通知设备绑定完成
/// @param callback 结果回调
- (void)bindCompletedWithCallback:(XMResultResponse)callback;

/// 配置WiFi
/// @param info WiFi信息，一般需要包含ssid、pwd、token
/// @param callback 结果回调
- (void)configWiFi:(NSDictionary *)info callback:(XMResultResponse)callback;

/// 通知设备断开WiFi
/// @param callback 结果回调
- (void)disconnectWiFiWithCallback:(XMResultResponse)callback;

#pragma mark - 废弃方法
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END

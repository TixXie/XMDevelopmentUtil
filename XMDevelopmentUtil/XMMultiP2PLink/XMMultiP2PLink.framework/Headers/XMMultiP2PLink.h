//
//  XMMultiP2PLink.h
//  XMMultiP2PLink
//
//  Created by Tix Xie on 2021/9/30.
//

#import <Foundation/Foundation.h>
//#import <XMMultiP2PLink/XMConnectManager.h>
#import <XMMultiP2PLink/XMMultiP2PLinkConstants.h>

NS_ASSUME_NONNULL_BEGIN

/// 版本号
FOUNDATION_EXPORT const unsigned char XMMultiP2PLinkVersionString[];

@protocol XMMultiP2PLinkDelegate <NSObject>
@optional
/// P2P信息回调
/// @param msg 信息
/// @param deviceName 设备名称 
/// @param type 回调类型
- (void)message:(NSString *)msg deviceName:(NSString *)deviceName type:(XMP2PType)type;

/// 视频数据回调
/// @param data 视频数据
/// @param deviceName 设备id
- (void)videoData:(NSData *)data deviceName:(NSString *)deviceName;

/// 接收设备自定义信息
/// @param data 信息数据
/// @param deviceName 设备名称
- (void)reviceDeviceCustomData:(NSData *)data deviceName:(NSString *)deviceName;
@end

@interface XMMultiP2PLink : NSObject
/// 代理回调
@property (nonatomic, weak) id <XMMultiP2PLinkDelegate> delegate;
/// 是否将数据帧写入文档
@property (nonatomic, assign) BOOL writeFile;
/// 日志输出等级，默认是XMP2PLogLevelInfo
@property (nonatomic, assign) XMP2PLogLevel logLevel;
/// 是否收集日志，不收集时需要关闭。调用- (void)collectLogs:filePath:开启或关闭
@property (nonatomic, readonly, assign, getter=isCollectLogs) BOOL collectLogs;
/// 日志保存路径
@property (nonatomic, readonly, copy) NSString *logFilePath;


/// 单例
/// @return XMMultiP2PLink实例
+ (instancetype)link;

/// 初始化P2P链路
/// @param pro_id 产品id
/// @param deviceName 连接的设备deviceName
/// @return XMP2P_ERR_NONE 是发送成功，其他错误码参考XMP2PErrCode枚举
- (XMP2PErrCode)startAppWith:(NSString *)pro_id deviceName:(NSString *)deviceName;

/// 设置设备p2pinfo
/// @param info p2pinfo
- (XMP2PErrCode)setDeviceP2PInfo:(NSString *)info;

/// 获取直播的播放链接
/// @param quality 视频质量
/// @param channel 通道
/// @return 播放的url
- (NSString *)getLiveUrlWithQuality:(XMP2PAVQuality)quality channel:(NSInteger)channel;

/// 获取回放的播放链接
/// @param startTime 视频的开始时间
/// @param endTime 视频的结束时间
/// @param channel 通道
/// @return 播放的url
- (NSString *)getPlaybackUrlWithStartTime:(NSUInteger)startTime endTime:(NSUInteger)endTime channel:(NSInteger)channel;

/// 开启音视频流（这个方法开启的音视频数据将在代理videoData:deviceName中回调）
/// @param type 直播 or 回放
/// @param channel 通道
/// @param quality 视频质量
- (void)startAvRecvService:(XMP2PAVType)type channel:(NSInteger)channel quality:(XMP2PAVQuality)quality;

/// 停止音视频流（对应startAvRecvService）
/// @return XMP2P_ERR_NONE 是发送成功，其他错误码参考XMP2PErrCode枚举
- (XMP2PErrCode)stopAvRecvService;

/// 开始语音对讲，并自动发送对讲音频
/// @param sampleRateType 采样率类型可选16000或8000，需要跟设备确定
/// @param deviceChannel 设备通道号，单机版可以填写0。（⚠️注意：这个不是音频的声道数！！！）
/// @param completion 异步开启录音，开启完成则回调
- (void)sendVoiceWithSampleRateType:(XMAudioSampleRateType)sampleRateType deviceChannel:(NSInteger)deviceChannel completion:(void(^)(void))completion;

/// 停止语音对讲
- (void)stopVoiceToServer;

/// 停止P2P服务
- (void)stopService;

/// 获取P2P版本号
+ (NSString *)getP2PVersion;

/// 收集日志
/// @param isCollect 是否收集 不收集时需要关闭
/// @param filePath 日志路径
- (void)collectLogs:(BOOL)isCollect filePath:(NSString *)filePath;

#pragma mark - 信令相关
/// 获取ipc设备状态,判断是否可以请求视频流(type区分直播(live)和对讲
/// @param checkType 类型 区分直播live和对讲voice
/// @param quality 视频质量
/// @param callback 回调
- (void)checkStatus:(XMP2PAVCheckType)checkType quality:(XMP2PAVQuality)quality callback:(XMDeviceStatusCallback)callback;

/// 获取ipc设备状态,判断是否可以请求视频流(type区分直播(live)和对讲
/// @param checkType 类型 区分直播live和对讲voice
/// @param quality 视频质量
/// @param channel 通道
/// @param timeout  超时时间，单位为微秒（比如设置5秒：5 * 1000 * 1000），值为0时采用默认超时(10s左右)
/// @param callback 回调
- (void)checkStatus:(XMP2PAVCheckType)checkType quality:(XMP2PAVQuality)quality channel:(NSUInteger)channel timeout:(NSUInteger)timeout callback:(XMDeviceStatusCallback)callback;

/// 获取本地录像列表
/// @param channel 通道号
/// @param startTime 开始时间/秒
/// @param endTime 结束时间/秒
/// @param callback 回调
- (void)localRecordListWithChannel:(NSUInteger)channel startTime:(NSUInteger)startTime endTime:(NSUInteger)endTime callback:(void(^)(NSDictionary *list, BOOL success))callback;

/// 下载设备中的文件
/// @param fileName 文件名称，下载事件视频时传入视频的：start_time
/// 数据在代理方法【videoData:deviceName:】中回调
/// 数据传输完成后在代理方法【message: deviceName: type:】中判断type == XMP2PTypeDownloadEnd；
- (void)downloadWithFileName:(NSString *)fileName;

#pragma mark - 测试相关
/// 登录(测试中)
/// @param username 用户名
/// @param password 密码
/// @param callback 回调
- (void)loginWithUsername:(NSString *)username password:(NSString *)password callback:(XMResultCallback)callback;

/// 测试接口：录制音视频流，保存到 document 目录 video.data 文件，需要开启writeFile = true
/// @param deviceName 连接的设备名称
- (void)recordWithDeviceName:(NSString *)deviceName;

/// 设置设备p2pinfo(测试中)
/// @param info p2pinfo
/// @param sec_id sec_id
/// @param sec_key sec_key
- (XMP2PErrCode)setDeviceP2PInfo:(NSString *)info sec_id:(NSString *)sec_id sec_key:(NSString *)sec_key;

#pragma mark - 废弃方法
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END


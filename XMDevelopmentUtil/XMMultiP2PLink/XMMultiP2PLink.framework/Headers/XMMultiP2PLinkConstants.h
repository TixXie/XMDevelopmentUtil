//
//  XMMultiP2PLinkConstants.h
//  XMMultiP2PLink
//
//  Created by Tix Xie on 2021/9/30.
//

#import <Foundation/Foundation.h>

/// P2P回调类型
typedef NS_ENUM(NSInteger, XMP2PType) {
    XMP2PTypeClose              = 1000,  // 数据传输完成
    XMP2PTypeCmd                = 1002,  // command json
    XMP2PTypeDisconnect         = 1003,  // p2p链路断开
    XMP2PTypeDetectReady        = 1004,  // p2p链路初始化成功
    XMP2PTypeDetectError        = 1005,  // p2p链路初始化失败
    XMP2PTypeDeviceMsgArrived   = 1006,  // 设备端向App发消息
    XMP2PTypeCmdNOReturn        = 1007,  // 设备未返回app自定义信令
    XMP2PTypeStreamEnd          = 1008,  // 设备停止推流，或者由于达到设备最大连接数，拒绝推流
    XMP2PTypeDownloadEnd        = 1009,  // 设备停止推流，下载结束
};

/// P2P关闭子类型
typedef NS_ENUM(NSInteger, XMP2PCloseSubType) {
    XMP2PVoiceServiceClose  = 2000,  // 语音对讲服务关闭
    XMP2PStreamServiceClose = 2001,  // 音视频流接收服务关闭
};

/// 日志输出等级
typedef NS_ENUM(NSInteger, XMP2PLogLevel) {
    XMP2PLogLevelDebug  = 0,  // 调试日志
    XMP2PLogLevelInfo   = 1,  // 一般日志
    XMP2PLogLevelWarn   = 2,  // 警告日志
    XMP2PLogLevelError  = 3,  // 错误日志
};

/// P2P错误码
typedef NS_ENUM(NSInteger, XMP2PErrCode) {
    XMP2P_ERR_NONE              = 0,        // 成功
    XMP2P_ERR_INIT_PRM          = -1000,    // 入参为空
    XMP2P_ERR_GET_XMP2PINFO     = -1001,    // SDK内部请求XMP2P info失败
    XMP2P_ERR_PROXY_INIT        = -1002,    // 本地p2p代理初始化失败
    XMP2P_ERR_UNINIT            = -1003,    // 数据接收/发送服务未初始化
    XMP2P_ERR_ENCRYPT           = -1004,    // 数据加密失败
    XMP2P_ERR_TIMEOUT           = -1005,    // 请求超时
    XMP2P_ERR_REQUEST_FAIL      = -1006,    // 请求错误
    XMP2P_ERR_VERSION           = -1007,    // 设备版本过低，请升级设备固件
    XMP2P_ERR_APPLICATION       = -1008,    // application初始化失败
    XMP2P_ERR_REQUEST           = -1009,    // request初始化失败
    XMP2P_ERR_DETECT_NOTREADY   = -1010,    // p2p探测未完成
    XMP2P_ERR_P2P_ININED        = -1011,    // 当前id对应的p2p已完成初始化
    XMP2P_ERR_P2P_UNININ        = -1012,    // 当前id对应的p2p未初始化
    XMP2P_ERR_NEW_MEMERY        = -1013,    // 内存申请失败
    XMP2P_ERR_XMP2PINFO_RULE    = -1014,    // 获取到的XMP2P info格式错误
    XMP2P_ERR_XMP2PINFO_DECRYPT = -1015,    // 获取到的XMP2P info解码失败
    XMP2P_ERR_PROXY_LISTEN      = -1016,    // 本地代理监听端口失败
    XMP2P_ERR_CLOUD_EMPTY       = -1017,    // 云端返回空数据
    XMP2P_ERR_JSON_PARSE        = -1018,    // json解析失败
    XMP2P_ERR_SERVICE_NOTRUN    = -1019,    // 当前id对应的服务(语音、直播等服务)没有在运行
    XMP2P_ERR_CLIENT_NULL       = -1020     // 从map中取出的client为空
};

/// 视频流类型
typedef NS_ENUM(NSInteger, XMP2PAVType) {
    XMP2PAVTypeLive     = 0,    // 直播
    XMP2PAVTypePlayback = 1,    // 回放
};

/// 设备状态
typedef NS_ENUM(NSInteger, XMDeviceStatus) {
    XMDeviceStatusReceive       = 0,    // 接收请求
    XMDeviceStatusReject        = 1,    // 拒绝请求
    XMDeviceStatusError         = 404,  // 错误请求
    XMDeviceStatusLimit         = 405,  // 连接APP数量超过最大连接数
    XMDeviceStatusNotSupported  = 406,  // 信令不支持
};

/// 视频质量
typedef NS_ENUM(NSInteger, XMP2PAVQuality) {
    XMP2PAVQualityStandard  = 0,    // 标准
    XMP2PAVQualityHigh      = 1,    // 高清
    XMP2PAVQualitySuper     = 2,    // 超清
};

/// 检查设备状态类型
typedef NS_ENUM(NSInteger, XMP2PAVCheckType) {
    XMP2PAVCheckTypeLive  = 0,    // 直播
    XMP2PAVCheckTypeVoice = 1,    // 对讲
};

/// 采样率类型
typedef NS_ENUM(NSInteger, XMAudioSampleRateType) {
    XMAudioSampleRateType_8k    = 0,    // 8000采样率
    XMAudioSampleRateType_16k   = 1,    // 16000采样率
};

/// 命令类型
typedef NS_ENUM(NSInteger, XMCommandType) {
    XMCommandTypeLogin = 0,             // 登录
};

@class XMDeviceStatusItem;

/// ⚠️注意：回调里面的code是信令下发结果不是设备回复的结果，设备回复结果在jsonList里面
typedef void(^XMCommandCallback)(NSString *jsonList, XMP2PErrCode code);

/// 查询设置状态结果回调，code是信令下发结果
typedef void(^XMDeviceStatusCallback)(NSArray <XMDeviceStatusItem *>*items, XMP2PErrCode code);

/** parameters结构如下：
 * @code
 {
     "result" : "ok", // failed
     "code" : 0,
     "msg" : "",
     "payload" : {
         "status" : 1,
     }
 }
 * @endcode
 * result 结果，ok为成功，failed为失败
 * code 错误码，0是成功
 * msg 错误信息，可能为空
 * payload 负载数据，可能为空
 */
typedef void(^XMResultCallback)(NSDictionary *parameters, XMP2PErrCode code);

@interface XMDeviceStatusItem : NSObject
/// 设备状态，对应XMDeviceStatus枚举
@property (nonatomic, copy) NSString *status;
/// 已连接到设备的APP数量
@property (nonatomic, copy) NSString *appConnectNum;
/// 是否接收信令
@property (nonatomic, assign, getter=isReceive) BOOL receive;
/// 视频记录列表，表示32位的数字，从低位到高位每一比特代表月份的第几天是否有录像；例如：8320（0010000010000000）表示8号和14号有录像；
@property (nonatomic, copy) NSString *video_list;

- (instancetype)initWithData:(NSDictionary *)dictionary;

@end


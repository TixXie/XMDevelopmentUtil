//
//  XMMQTTSession.h
//  Cuco
//
//  Created by Tix Xie on 2020/5/16.
//  Copyright © 2020 TixXie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XMMQTTClient/XMMQTTSessionConstants.h>

/// 收到新的MQTT消息的通知, object 是 XMMQTTResponse 或者是其子类 对象
FOUNDATION_EXPORT NSString *const XMMQTTDidReceiveMessageNotification;

@class XMMQTTSession;
@protocol XMMQTTSessionDelegate <NSObject>
@optional

/// 消息发送成功回调，qos0没有回调
/// @param session XMMQTTSession
/// @param seq 消息ID
- (void)messageDelivered:(XMMQTTSession *)session seq:(NSInteger)seq;

/// 收到新的MQTT消息，与XMMQTTDidReceiveMessageNotification通知一样
/// @param session XMMQTTSession
/// @param response 消息对象，使用其子类
- (void)XMMQTTSession:(XMMQTTSession *)session didReceiveMessage:(XMMQTTResponse *)response;

@end
@interface XMMQTTSession : NSObject
/// 代理
@property (nonatomic, weak) id <XMMQTTSessionDelegate> delegate;
/// MQTT状态
@property (nonatomic, readonly, assign) XMMQTTSessionStatus status;
/// 超时时间，单位秒，默认20秒
@property (nonatomic, assign) NSUInteger timeout;
/// 断开自动重连 默认YES，调用connectWithItem后也是YES 调用disconnect后为NO
@property (nonatomic, assign, getter=isAutoReconnect) BOOL autoReconnect;
/// 订阅
@property (strong, nonatomic) NSDictionary<NSString *, NSNumber *> *subscriptions;

/// 单例
+ (instancetype)session;

/// 设置日志输出等级
/// @param level 等级
+ (void)setLogLevel:(XMMQTTLogLevel)level;

/// 连接MQTT服务器
/// @param item 连接参数
- (void)connectToItem:(XMMQTTConnectItem *)item;

/// 断开连接MQTT
- (void)disconnect;

/// 发送参数
/// @param item 参数对象
/// return 返回消息 seq，负数代表失败
- (NSInteger)sendData:(XMMQTTSendItem *)item;

/// 发送自定义数据
/// @param item  自定义数据对象
- (void)sendCustomItem:(XMMQTTSendCustomItem *)item;

#pragma mark - 废弃方法
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

@end

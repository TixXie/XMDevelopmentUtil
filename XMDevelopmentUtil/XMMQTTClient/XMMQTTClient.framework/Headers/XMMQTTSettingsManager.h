//
//  XMMQTTSettingsManager.h
//  Lindo
//
//  Created by Tix Xie on 2022/4/26.
//  Copyright © 2022 TixXie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XMMQTTClient/XMMQTTSession.h>
#import <XMMQTTClient/XMMQTTSessionConstants.h>

/// 结果回调block
typedef void (^XMResultResponse)(XMMQTTReplyResponse *res);

typedef NS_ENUM(NSInteger, TIXDeviceWakeupMode) {
    /// 直播
    TIXDeviceWakeupModeLive     = 1,
    /// 回放
    TIXDeviceWakeupModePlayback = 2,
};

@interface XMMQTTSettingsManager : NSObject

/// 单例
+ (instancetype)shared;

/// 配置设备参数
/// @param payload 参数数据，key是属性，value是值
/// @param parameters 设备参数结构体
/// @param callback 回调
- (void)config:(NSDictionary *)payload parameters:(XMMQTTProductParameters *)parameters callback:(XMResultResponse)callback;

/// 发送Action命令
/// @param actionId actionId
/// @param payload 负载
/// @param parameters 设备参数结构体
/// @param callback 回调
/// return seq
- (NSInteger)sendActionId:(NSString *)actionId payload:(NSDictionary *)payload parameters:(XMMQTTProductParameters *)parameters callback:(XMResultResponse)callback;

/// 唤醒设备
/// @param mode 1：Live（直播视频） 2：Play （回放录像）
/// @param parameters 设备参数结构体
/// @param callback 回调
/// return seq
- (NSInteger)wakeupMode:(TIXDeviceWakeupMode)mode parameters:(XMMQTTProductParameters *)parameters callback:(XMResultResponse)callback;

/// 重置设备
/// @param parameters 设备参数结构体
/// @param callback 回调
- (void)reset:(XMMQTTProductParameters *)parameters callback:(XMResultResponse)callback;

/// 重启设备
/// @param parameters 设备参数结构体
/// @param callback 回调
- (void)reboot:(XMMQTTProductParameters *)parameters callback:(XMResultResponse)callback;

/// 切换网络
/// @param ssid ssid
/// @param password 密码
/// @param parameters 设备参数结构体
/// @param callback 回调
- (void)switchNetworkToSSID:(NSString *)ssid password:(NSString *)password parameters:(XMMQTTProductParameters *)parameters callback:(XMResultResponse)callback;

/// 让设备检测OTA
/// @param parameters 设备参数结构体
/// @param callback 回调
- (void)checkOTA:(XMMQTTProductParameters *)parameters callback:(XMResultResponse)callback;

/// 播放快捷回复
/// @param parameters 设备参数结构体
/// @param quick_id 快捷回复ID
/// @param callback 回调
- (void)playQuickReply:(XMMQTTProductParameters *)parameters quickId:(NSUInteger)quick_id callback:(XMResultResponse)callback;
@end


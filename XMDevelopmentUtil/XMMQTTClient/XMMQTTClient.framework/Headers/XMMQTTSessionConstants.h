//
//  XMMQTTSessionConstants.h
//  Lindo
//
//  Created by Tix Xie on 2022/4/25.
//  Copyright © 2022 TixXie. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Send Method List
/// 属性消息
FOUNDATION_EXPORT NSString *const XMMQTTMethodAppControl;
/// 行为消息
FOUNDATION_EXPORT NSString *const XMMQTTMethodAppAction;

#pragma mark - Receive Method List
/// 属性消息回复
FOUNDATION_EXPORT NSString *const XMMQTTMethodAppControlReply;
/// 行为消息回复
FOUNDATION_EXPORT NSString *const XMMQTTMethodAppActionReply;
/// 同步属性消息
FOUNDATION_EXPORT NSString *const XMMQTTMethodPropertySync;

#pragma mark - Event Notify ID List
/// 事件通知消息
FOUNDATION_EXPORT NSString *const XMMQTTMethodEventNotify;
/// 绑定设备结果通知
FOUNDATION_EXPORT NSString *const XMMQTTMethodNotifyIdBindDevice;
/// 设备固件升级状态通知
FOUNDATION_EXPORT NSString *const XMMQTTMethodNotifyIdUpgradeStatus;

/// 设备通知
FOUNDATION_EXPORT NSString *const XMMQTTMethodDeviceNotify;
/// 设备在线状态通知
FOUNDATION_EXPORT NSString *const XMMQTTMethodNotifyIdOnlineStatus;
/// 设备属性同步（物模型）
FOUNDATION_EXPORT NSString *const XMMQTTMethodNotifyIdDeviceProperty;

#pragma mark - LOG
/// 服务器通知上传日志
FOUNDATION_EXPORT NSString *const XMMQTTMethodLogcatNotify;

/// MQTT状态
typedef NS_ENUM(NSInteger, XMMQTTSessionStatus) {
    /// 创建
    XMMQTTSessionStatusCreated,
    /// 连接中
    XMMQTTSessionStatusConnecting,
    /// 已连接
    XMMQTTSessionStatusConnected,
    /// 正在断开连接
    XMMQTTSessionStatusDisconnecting,
    /// 关闭
    XMMQTTSessionStatusClosed,
    /// 错误
    XMMQTTSessionStatusError
};

/// MQTT错误码
typedef NS_ENUM(NSInteger, XMMQTTErrorType) {
    /// 成功
    XMMQTTErrorTypeSuccess         = 0,
    /// 参数错误
    XMMQTTErrorTypeParameter       = -1,
    /// 超时
    XMMQTTErrorTypeTimeout         = -999,
    /// 主动取消
    XMMQTTErrorTypeCancel           = -9999,
};

/// 日志输出等级
typedef NS_ENUM(NSInteger, XMMQTTLogLevel) {
    /// 调试日志
    XMMQTTLogLevelDebug  = 0,
    /// 一般日志
    XMMQTTLogLevelInfo   = 1,
    /// 警告日志
    XMMQTTLogLevelWarn   = 2,
    /// 错误日志
    XMMQTTLogLevelError  = 3,
};

/// 设置设备参数时需要的设备信息
@interface XMMQTTProductParameters : NSObject
/// 设备SN
@property (nonatomic, copy) NSString *sn;
/// 产品ID
@property (nonatomic, copy) NSString *productId;

- (instancetype)initWithProductId:(NSString *)productId sn:(NSString *)sn;
@end

@interface XMMQTTConnectItem : NSObject
/// mqtt域名端口
@property (nonatomic, assign) int port;
/// 客户端id
@property (nonatomic, copy) NSString *client_id;
/// mqtt 域名地址
@property (nonatomic, copy) NSString *host;
/// mqtt 连接用户名
@property (nonatomic, copy) NSString *username;
/// mqtt 连接密码
@property (nonatomic, copy) NSString *password;
/// 默认订阅主题
@property (nonatomic, copy) NSString *subscribe;
/// 默认发布主题
@property (nonatomic, copy) NSString *publish;
@end


#pragma mark - 接收消息对象
/// 默认响应对象，用子类
@interface XMMQTTResponse : NSObject
/// 响应method，具体类型可查看 Receive Method List
@property (nonatomic, copy) NSString *method;
/// 响应的负载数据
@property (nonatomic, copy) NSDictionary *payload;
/// 其他信息
@property (nonatomic, copy) NSDictionary *expands;
/// 响应的topic
@property (nonatomic, copy) NSString *topic;
/// 事件id
@property (nonatomic, copy) NSString *seq;
/// 响应时间
@property (nonatomic, assign) NSInteger timestamp;

- (instancetype)initWithData:(NSDictionary *)data;

@end

/// 回复响应
@interface XMMQTTReplyResponse : XMMQTTResponse
/// 响应的信息
@property (nonatomic, copy) NSString *msg;
/// 状态码，0代表成功
@property (nonatomic, assign) NSInteger code;
/// 结果，如果是NO，请查看code对应错误码
@property (nonatomic, assign, getter=isSuccess) BOOL result;
@end

/// 控制回复
@interface XMMQTTReplyControlResponse : XMMQTTReplyResponse

@end

/// 行为回复
@interface XMMQTTReplyActionResponse : XMMQTTReplyResponse
/// 响应的actionId
@property (nonatomic, copy) NSString *actionId;
@end

/// 参数同步
@interface XMMQTTSyncResponse : XMMQTTResponse
/// 设备SN
@property (nonatomic, copy) NSString *sn;
/// 产品ID
@property (nonatomic, copy) NSString *productId;
@end

/// 事件通知
@interface XMMQTTNotifyResponse : XMMQTTReplyResponse
/// 通知ID
@property (nonatomic, copy) NSString *notifyId;

@end

/// 设备通知
@interface XMMQTTDeviceNotifyResponse : XMMQTTNotifyResponse

@end

/// 日志通知
@interface XMMQTTLogcatNotifyResponse : XMMQTTNotifyResponse

@end

#pragma mark - 发送消息对象
/// 发送的消息对象，用子类型
@interface XMMQTTSendItem : NSObject
/// 发送的消息类型，具体可查看Send Method List
@property (nonatomic, copy) NSString *method;
/// 产品ID
@property (nonatomic, copy) NSString *productId;
/// 设备SN
@property (nonatomic, copy) NSString *sn;
/// 发送的主题，默认连接时填写的publish
@property (nonatomic, copy) NSString *topic;
/// 发送的QOS
@property (nonatomic, assign) NSInteger qos;
/// 需要发送的参数，具体参数请查看网页文档
@property (nonatomic, copy) NSDictionary *payload;

- (instancetype)initWithMethod:(NSString *)method productId:(NSString *)productId sn:(NSString *)sn topic:(NSString *)topic qos:(uint)qos payload:(NSDictionary *)payload;
@end


/// 属性控制消息对象
@interface XMMQTTSendControlItem : XMMQTTSendItem

@end


/// 行为消息对象
@interface XMMQTTSendActionItem : XMMQTTSendItem
/// 行为ID
@property (nonatomic, copy) NSString *actionId;
@end

@interface XMMQTTSendCustomItem : NSObject

/// 发送的主题，默认连接时填写的publish
@property (nonatomic, copy) NSString *topic;
/// 发送的QOS
@property (nonatomic, assign) NSInteger qos;
/// 发送的数据
@property (nonatomic, copy) NSData *data;

- (instancetype)initWithTopic:(NSString *)topic qos:(uint)qos data:(NSData *)data;

@end

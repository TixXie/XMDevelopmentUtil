# XMDevelopmentUtil

[![CI Status](https://img.shields.io/travis/TixXie/XMDevelopmentUtil.svg?style=flat)](https://travis-ci.org/TixXie/XMDevelopmentUtil)
[![Version](https://img.shields.io/cocoapods/v/XMDevelopmentUtil.svg?style=flat)](https://cocoapods.org/pods/XMDevelopmentUtil)
[![License](https://img.shields.io/cocoapods/l/XMDevelopmentUtil.svg?style=flat)](https://cocoapods.org/pods/XMDevelopmentUtil)
[![Platform](https://img.shields.io/cocoapods/p/XMDevelopmentUtil.svg?style=flat)](https://cocoapods.org/pods/XMDevelopmentUtil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XMDevelopmentUtil is available through [CocoaPods](https://cocoapods.org). To install it, simply add the following line to your Podfile:

##### 导入所有库
```ruby
pod 'XMDevelopmentUtil'
```

##### 导入MQTT组件
```ruby
pod 'XMDevelopmentUtil/XMMQTTClient'
```
MQTT组件编译注意事项：
1. 需要新建一个空的Swift文件，如Example中的Empty.swift，可以不创建桥接文件。

##### 导入蓝牙组件
```ruby
pod 'XMDevelopmentUtil/XMBLESession'
```

##### 导入腾讯iot组件
```ruby
pod 'XMDevelopmentUtil/XMMultiP2PLink'
```

## Author

TixXie, x.maple@hotmail.com

## License

XMDevelopmentUtil is available under the Apache License 2.0 license. See the LICENSE file for more info.

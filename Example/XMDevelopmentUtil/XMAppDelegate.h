//
//  XMAppDelegate.h
//  XMDevelopmentUtil
//
//  Created by TixXie on 10/20/2022.
//  Copyright (c) 2022 TixXie. All rights reserved.
//

@import UIKit;

@interface XMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

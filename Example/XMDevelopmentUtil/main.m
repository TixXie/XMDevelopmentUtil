//
//  main.m
//  XMDevelopmentUtil
//
//  Created by TixXie on 10/20/2022.
//  Copyright (c) 2022 TixXie. All rights reserved.
//

@import UIKit;
#import "XMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XMAppDelegate class]));
    }
}
